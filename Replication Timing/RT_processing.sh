#save file names in a file 

For i in *_R1.fastq.gz; do echo ${i/%_R1.fastq.gz/} >> basenames.txt

# run trim_galore
for h in E L ; do for i in $(cat basenames.txt); do echo $i'_'$h; trim_galore $i'_'$h'_R1.fastq.gz'; done & done

#align using bowtie2, filter and sort with samtools

for h in E L ; do for i in $(cat basenames.txt); do echo $i'_'$h; bowtie2 -k 1 --threads 8 -x ~/mm10_ref_genome/mm10 -U $i'_'$h'_R1_trimmed.fq.gz' |samtools view -bSu -F 0x4 - | samtools sort -@8 -O BAM -o $i'_'$h'_sorted.bam' - ; done ; done

#remove duplicates using piacard markduplicates 
for i in *.bam; do echo $i ; picard.jar MarkDuplicates I=$i O=${i/.bam/_delDup.bam} M=${i/.bam/_delDup.log} REMOVE_DUPLICATES=true ASSUME_SORTED=true VALIDATION_STRINGENCY=LENIENT USE_JDK_DEFLATER=true USE_JDK_INFLATER=true ; done

# compute log2 bedgraph usign deeptools
# 1Kb
for i in $(cat basenames.txt); do bamCompare -b1 $i'_E_sorted_delDup.bam' -b2 $i'_L_sorted_delDup.bam' -p 8 -bs 1000 --scaleFactorsMethod None --normalizeUsing CPM -of bedgraph  -o bedgraph_RPM/$i'_dedup_log2_EL_1kb.bedGraph' & done
# 50Kb
for i in $(cat basenames.txt); do bamCompare -b1 $i'_E_sorted_delDup.bam' -b2 $i'_L_sorted_delDup.bam' -p 8 -bs 50000 --scaleFactorsMethod None --normalizeUsing CPM -of bedgraph  -o bedgraph_RPM/$i'_dedup_log2_EL_50kb.bedGraph' & done
